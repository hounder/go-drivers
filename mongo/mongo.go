package mongo

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Connection struct {
	Client     *mongo.Client
	Collection *mongo.Collection
}

func NewConnection(connectionString, dbName, collectionName string) (*Connection, error) {
	clientOptions := options.Client().ApplyURI(connectionString)

	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		return nil, err
	}

	if err = client.Ping(context.TODO(), nil); err != nil {
		return nil, err
	}

	collection := client.Database(dbName).Collection(collectionName)

	return &Connection{Client: client, Collection: collection}, nil
}
