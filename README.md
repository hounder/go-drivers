# Go Drivers

A set of minimal Go wrappers to interact with different low level libraries:

- RabbitMQ simple consumer with reconnection handling
- Chrome screnshotter
- MaxmindDB reader for ASN and City information
- MongoDB connection
- S3 Bucket uploader
