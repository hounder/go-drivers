package rabbitmq

import (
	"context"
	"errors"

	amqp "github.com/rabbitmq/amqp091-go"
)

type Publisher struct {
	name       string
	connection *amqp.Connection
	channel    *amqp.Channel
	exchange   string
	uri        string
	err        chan error
}

func NewPublisher(uri, exchange, name string) *Publisher {
	return &Publisher{
		name:  name,
		exchange: exchange,
		uri:   uri,
		err:   make(chan error),
	}
}

func (p *Publisher) Connect() error {
	var err error

	config := amqp.Config{Properties: amqp.NewConnectionProperties()}
	config.Properties.SetClientConnectionName(p.name)
	p.connection, err = amqp.DialConfig(p.uri, config)
	if err != nil {
		return err
	}

	// Listen to NotifyClose for handling connection recovery
	go func() {
		<-p.connection.NotifyClose(make(chan *amqp.Error))
		p.err <- errors.New("AMQP connection closed")
	}()

	p.channel, err = p.connection.Channel()
	if err != nil {
		return err
	}

	if err = p.channel.ExchangeDeclare(
                p.exchange,    // name
                "direct",      // type
                true,          // durable
                false,         // auto-deleted
                false,         // internal
                false,         // no-wait
                nil,           // arguments
        ); err != nil {
		return err
	}

	return nil
}

// Publish publishes a message to the exchange with the specified routing key
func (p *Publisher) Publish(message []byte, routingKey string) error {
	select { //non blocking channel - if there is no error will go to default where we do nothing
	case err := <-p.err:
		if err != nil {
			p.Connect()
		}
	default:
	}

	if err := p.channel.PublishWithContext(
		context.Background(),
                p.exchange,         // exchange
                routingKey, // routing key
                false, // mandatory
                false, // immediate
                amqp.Publishing{
                        ContentType: "application/json",
                        Body:        message,
                },
	); err != nil {
		return err
	}
	return nil
}

func (c *Publisher) Close() {
	c.channel.Close()
	c.connection.Close()
}

