package rabbitmq

import (
	"errors"
	"log"

	amqp "github.com/rabbitmq/amqp091-go"
)

type Consumer struct {
	name       string
	connection *amqp.Connection
	channel    *amqp.Channel
	exchange      string
	queue      string
	bindings []string
	uri        string
	err        chan error
}

func NewConsumer(uri, exchange, queue, name string, bindings []string) *Consumer {
	return &Consumer{
		name:  name,
		exchange: exchange,
		queue: queue,
		bindings: bindings,
		uri:   uri,
		err:   make(chan error),
	}
}

func (c *Consumer) Connect() error {
	var err error

	config := amqp.Config{Properties: amqp.NewConnectionProperties()}
	config.Properties.SetClientConnectionName(c.name)
	c.connection, err = amqp.DialConfig(c.uri, config)
	if err != nil {
		return err
	}

	// Listen to NotifyClose for handling connection recovery
	go func() {
		<-c.connection.NotifyClose(make(chan *amqp.Error))
		c.err <- errors.New("AMQP connection closed")
	}()

	c.channel, err = c.connection.Channel()
	if err != nil {
		return err
	}

	if err = c.channel.ExchangeDeclare(
                c.exchange,    // name
                "direct",      // type
                true,          // durable
                false,         // auto-deleted
                false,         // internal
                false,         // no-wait
                nil,           // arguments
        ); err != nil {
		return err
	}

	q, err := c.channel.QueueDeclare(
		c.queue,    // name
		true,       // durable
		false,      // delete when unused
		false,      // exclusive
		false,      // no-wait
		nil,        // arguments
		)
	if err != nil {
		return err
	}
	for _, s := range c.bindings {
		err = c.channel.QueueBind(
			q.Name,        // queue name
			s,             // routing key
			c.exchange,    // exchange
			false,
			nil)
		if err != nil {
			return err
		}
	}

	if err := c.channel.Qos(1, 0, false); err != nil {
		return err
	}

	return nil
}

func (c *Consumer) ConsumeMessages() (<-chan amqp.Delivery, error) {
	deliveries, err := c.channel.Consume(c.queue, "", false, false, false, false, nil)
	if err != nil {
		return nil, err
	}
	return deliveries, nil
}

func (c *Consumer) HandleConsumedDeliveries(delivery <-chan amqp.Delivery, handler func(<-chan amqp.Delivery)) {
	for {
		go handler(delivery)
		if err := <-c.err; err != nil {
			c.Connect()
			delivery, err = c.ConsumeMessages()
			if err != nil {
				log.Fatalf("Could not recover AMQP connection: %s", err.Error())
			}
			log.Println("Recovered lost AMQP connection")
		}
	}
}

func (c *Consumer) Close() {
	c.channel.Close()
	c.connection.Close()
}

