package spaces

import (
	"fmt"
	"io"
	"net/url"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

type StorageClient struct {
	bucket string
	client *s3.S3
	config *aws.Config
}

func NewStorageClient(bucket, endpoint, region, key, secret string) (*StorageClient, error) {
	u, err := url.ParseRequestURI(endpoint)
	if err != nil {
		return nil, err
	}

	config := &aws.Config{
		Credentials:      credentials.NewStaticCredentials(key, secret, ""),
		Endpoint:         aws.String(endpoint),
		Region:           aws.String(region),
		DisableSSL:       aws.Bool(u.Scheme == "http"),
		S3ForcePathStyle: aws.Bool(true),
	}
	session, err := session.NewSession(config)
	if err != nil {
		return nil, err
	}

	client := s3.New(session)
	return &StorageClient{bucket: bucket, client: client, config: config}, nil
}

func (c *StorageClient) PutObject(id string, data io.ReadSeeker) (*s3.PutObjectOutput, error) {
	screenshotName := fmt.Sprintf("screenshots/%s.png", id)
	object := s3.PutObjectInput{
		Bucket: aws.String(c.bucket),
		Key:    aws.String(screenshotName),
		Body:   data,
		ACL:    aws.String("public-read"),
		Metadata: map[string]*string{
			"x-amz-meta-my-key": aws.String(id),
		},
	}
	result, err := c.client.PutObject(&object)
	if err != nil {
		return nil, err
	}

	return result, nil
}
