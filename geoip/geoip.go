package geoip

import (
	"fmt"
	"net"

	"github.com/oschwald/maxminddb-golang"
)

type Reader struct {
	ASNReader  *maxminddb.Reader
	CityReader *maxminddb.Reader
}

type Record struct {
	ASNNumber       int    `maxminddb:"autonomous_system_number" bson:"asn"`
	ASNOrganization string `maxminddb:"autonomous_system_organization" bson:"organization"`
	Country         struct {
		ISOCode string `maxminddb:"iso_code" bson:"countryISOCode"`
		Names   struct {
			EN string `maxminddb:"en" bson:"countryName"`
		} `maxminddb:"names" bson:"inline"`
	} `maxminddb:"country" bson:"inline"`
	Location struct {
		Latitude  float64 `maxminddb:"latitude" bson:"latitude"`
		Longitude float64 `maxminddb:"longitude" bson:"longitude"`
	} `maxminddb:"location" bson:"location"`
	City struct {
		Names struct {
			EN string `maxminddb:"en" bson:"cityName,omitempty"`
		} `maxminddb:"names" bson:"inline"`
	} `maxminddb:"city" bson:"inline"`
}

func NewReader(path string) (*Reader, error) {
	asnReader, err := maxminddb.Open(fmt.Sprintf("%s/GeoLite2-ASN.mmdb", path))
	if err != nil {
		return nil, err
	}

	cityReader, err := maxminddb.Open(fmt.Sprintf("%s/GeoLite2-City.mmdb", path))
	if err != nil {
		return nil, err
	}

	return &Reader{ASNReader: asnReader, CityReader: cityReader}, nil
}

func (g *Reader) GetInfo(ip net.IP) (*Record, error) {
	var record Record

	if err := g.ASNReader.Lookup(ip, &record); err != nil {
		return nil, err
	}

	if err := g.CityReader.Lookup(ip, &record); err != nil {
		return nil, err
	}

	return &record, nil
}

func (g *Reader) Close() {
	g.ASNReader.Close()
	g.CityReader.Close()
}
